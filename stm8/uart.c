#include "uart.h"
#include "stm8s.h"

// TODO pass in the UART id as an initialiser
void uart_init(const uint16_t baudrate, const bool enable_rx_interrupt) {
    /* round to nearest integer */
    uint16_t div = (F_CPU + baudrate / 2) / baudrate;
    /* madness.. */
    UART2_BRR2 = ((div >> 8) & 0xF0) + (div & 0x0F);
    UART2_BRR1 = div >> 4;

	if (enable_rx_interrupt) {
		UART2_CR2 = 1 << UART2_CR2_RIEN;
	}

    /* enable transmitter and receiver */
    UART2_CR2|= (1 << UART2_CR2_TEN) | (1 << UART2_CR2_REN);
}

void uart_write(uint8_t data) {
    UART2_DR = data;
    while (!(UART2_SR & (1 << UART2_SR_TC)));
}

uint8_t uart_read() {
    while (!(UART2_SR & (1 << UART2_SR_RXNE)));
    return UART2_DR;
}

bool uart_read_available() {
	return UART2_SR & (1 << UART2_SR_RXNE);
}
